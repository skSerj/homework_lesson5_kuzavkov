package com.company.task1;

import com.company.task1.data.BookData;
import com.company.task1.model.BooksFinder;

public class Task1 {
    public void run() {
        BooksFinder booksFinder = new BooksFinder();
        booksFinder.books = new BookData().initBooks();
        booksFinder.finderByAuthors();
        booksFinder.finderByMinYearOfPublishing();
        booksFinder.finderByMaxYearofPublishing();
        booksFinder.finderByMinQuantityAuthors();
        booksFinder.finderbyYearOfPublishing();
    }
}