package com.company.task1.model;

public class Book {
    private String title;
    private String authorName;
    private String publishingHouse;
    private int yearOfPublishing;

    public Book(String title, String authorName, String publishingHouse, int yearOfPublishing) {
        this.title = title;
        this.authorName = authorName;
        this.publishingHouse = publishingHouse;
        this.yearOfPublishing = yearOfPublishing;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getTitle() {
        return title;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public int getYearOfPublishing() {
        return yearOfPublishing;
    }
}

