package com.company.task1.model;

import com.company.task1.data.BookData;

public class BooksFinder {
    public Book[] books = new BookData().initBooks();

    int findYearOfPublishing = 2006;
    String findAuthor = "Таран";
    int maxYearOfPublishingToFind = 1990;
    int minYearOfPublishing = books[0].getYearOfPublishing();
    int minQuantityOfAuthors = 3;

    public void finderbyYearOfPublishing() {
        for (Book a : books) {
            if (a.getYearOfPublishing() == findYearOfPublishing) {
                System.out.println("Искомая книга по году издания: " + a.getTitle() + "; Автор: " + a.getAuthorName());
            }
        }
    }

    public void finderByAuthors() {
        for (Book i : books) {
            if (i.getAuthorName().toUpperCase().contains(findAuthor.toUpperCase())) {
                System.out.println("Искомой книгой по автору " + findAuthor + " , является : " + i.getTitle() + ". Автор(авторы): " + i.getAuthorName() + ", издательство: " + i.getPublishingHouse() + ", " + i.getYearOfPublishing());
            }
        }
    }

    public void finderByMinQuantityAuthors() {
        for (Book z : books) {
            if (z.getAuthorName().split(",").length > minQuantityOfAuthors) {
                System.out.println("Книга с количеством авторов больше " + minQuantityOfAuthors + ": Название: " + z.getTitle() + ", авторы: " + z.getAuthorName() + ", издательство: " + z.getPublishingHouse() + ", " + z.getYearOfPublishing());
            }

        }
    }

    public void finderByMinYearOfPublishing() {
        for (Book b : books) {
            if (b.getYearOfPublishing() < minYearOfPublishing) {
                minYearOfPublishing = b.getYearOfPublishing();
            }
        }
        System.out.println("самая ранняя книга со списка была издана в " + minYearOfPublishing + " году");
    }

    public void finderByMaxYearofPublishing() {
        for (Book c : books) {
            if (c.getYearOfPublishing() < maxYearOfPublishingToFind) {
                System.out.println("книга выпущенная ранне " + maxYearOfPublishingToFind + " года: " + c.getTitle() + "; автор: " + c.getAuthorName() + ", " + c.getYearOfPublishing() + " г.");
            }
        }
    }
}
