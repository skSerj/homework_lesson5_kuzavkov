package com.company.task2;

import com.company.task2.data.Generator;
import com.company.task2.model.BanksHolder;

import java.util.Scanner;

public class Task2 {
    public void run() {
        BanksHolder banksHolders = new BanksHolder(new Generator().generate());

        Scanner scan = new Scanner(System.in);
        System.out.println("ваша операция Покупка или Продажа гривны?: ");
        String purchaseOrSale = scan.nextLine();
// enter amount of money
        System.out.println(" Введите сумму, которую Вы желаете конвертировать");
        int amount = Integer.parseInt(scan.nextLine());
// enter currency
        System.out.println("введите валюту (usd, EUR или RUB)");
        String currency = scan.nextLine();

        System.out.println("Введите название банка, в котором желаете произвести процедуру обмена (ПриватБанк, ОщадБанк или ПУМБ) :");
        String bankName = scan.nextLine();

// convert UAH to user currency
        banksHolders.calc(bankName, currency, amount, purchaseOrSale);
    }
}