package com.company.task2.model;

public class Currency {
    private String name;
    private float rateOfPurchase;
    private float rateOfSales;

    public Currency(String name, float rateOfPurchase, float rateOfSales) {
        this.name = name;
        this.rateOfPurchase = rateOfPurchase;
        this.rateOfSales = rateOfSales;
    }

    public String getName() {
        return name;
    }

    public float getRateOfPurshase() {
        return rateOfPurchase;
    }

    public float getRateOfSales() {
        return rateOfSales;
    }
}
