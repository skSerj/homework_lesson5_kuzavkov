package com.company.task2.model;

import com.company.task2.data.Generator;

import java.util.Locale;

public class BanksHolder {
    private Bank[] banks;

    public BanksHolder(Bank[] banks) {
        this.banks = banks;
    }

    public void calc(String bankName, String currency, int amount, String purchaseOrSale) {
        for (Bank currentBank : banks) {
            if (currentBank.isSameAs(bankName)) {
                currentBank.convert(currency, amount, purchaseOrSale);
            }
        }
    }
}

