package com.company.task2.model;

import java.util.Locale;

public class Bank {
    private String name;
    private Currency[] rates;
    String purchase = "покупка";
    String sale = "продажа";

    public Bank(String name, Currency[] rates) {
        this.name = name;
        this.rates = rates;
    }

    public boolean isSameAs(String bankName) {
        return name.equalsIgnoreCase(bankName);
    }


    public void convert(String currency, int amount, String purchaseOrSale) {
        for (Currency currentRate : rates) {
            if (currentRate.getName().equalsIgnoreCase(currency) && (purchase.equalsIgnoreCase(purchaseOrSale))) {
                System.out.println(String.format(Locale.US, "В случае обмена указанной валюты на гривну, вы получите :  %.2f", amount * currentRate.getRateOfSales()));
            } else if (currentRate.getName().equalsIgnoreCase(currency) && (sale.equalsIgnoreCase(purchaseOrSale))) {
                System.out.println(String.format(Locale.US, "Ваши деньги при обмене гривны на   %s: %.2f", currency, amount / currentRate.getRateOfPurshase()));
            }
        }
    }
}


