package com.company.task2.data;

import com.company.task2.model.Bank;
import com.company.task2.model.Currency;

public class Generator {
    public Generator() {
    }

    public Bank[] generate() {
        Bank[] banks = new Bank[3];
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.35F, 27.90F);
            currencies[1] = new Currency("eur", 29.65F, 30.50F);
            currencies[2] = new Currency("rub", 0.320F, 0.360F);
            banks[0] = new Bank("ПриватБанк", currencies);
        }
        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.25F, 28.00F);
            currencies[1] = new Currency("eur", 29.50F, 30.50F);
            currencies[2] = new Currency("rub", 0.330F, 0.350F);
            banks[1] = new Bank("ПУМБ", currencies);
        }

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 27.25F, 28.20F);
            currencies[1] = new Currency("eur", 29.20F, 30.60F);
            currencies[2] = new Currency("rub", 0.230F, 0.380F);
            banks[2] = new Bank("ОщадБанк", currencies);
        }

        return banks;
    }

}
